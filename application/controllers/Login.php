<?php 

Class Login extends CI_Controller
{
	function __construct()
    {
        header('Cache-Control: no-store,no-cache,must-revalidate');    
        header('Cache-Control: post-check=0,pre-check=0',false);       
        header('Pragma:no-cache');
            
        parent::__construct();
        $this->load->model('loginmodel');
            
        
    } 

	function index()
    {
        if($this->input->post())
        {
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
            $email = $this->input->post('email');
            #$password = $this->input->post('password');
            $password = md5($this->input->post('password'));

            if ($this->form_validation->run() == FALSE){
                $this->load->view('login');
            }else{
  
                $result = $this->loginmodel->signin($email,$password);
                // print_r($result); die();
                if($result){
                    $session_admin_data = array(
                    'superid' => $result->id,
                    'supername' => $result->name);
                    $this->session->set_userdata($session_admin_data);
                    
                    redirect('dashboard'); 
                }else{
                    $this->session->set_flashdata('msg','<p style="color:red;">*Invalid Username or Password.</p>');
                   
                }
            }
        }
        
        $this->load->view('login');
    
    }


     public function logout() {
        
        $this->session->sess_destroy();
        redirect('Login');

    }
    


}




?>