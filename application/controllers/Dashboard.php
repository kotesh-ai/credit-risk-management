<?php 

Class Dashboard extends CI_Controller{

	function __construct()
    {
        header('Cache-Control: no-store,no-cache,must-revalidate');    
        header('Cache-Control: post-check=0,pre-check=0',false);       
        header('Pragma:no-cache');
            
        parent::__construct();
        
         $this->data['page'] = 'dashboard';
       if($this->session->userdata('superid') =='' || (!$this->session->userdata('superid')))
        {
            redirect('login');
        } 
    }

    function index()
    {

		$this->data['page_title'] = "Dashboard";
		$this->load->view('dashboard',$this->data);
       
    } 
}

?>