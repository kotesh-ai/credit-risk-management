 <?php defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends CI_Controller
{
	
	function __construct() {
        parent::__construct();
        $this->data['page'] = 'Profit&Loss';
        // $this->login_required();
        // $this->data['page_unique_name'] = 'categories';
        // $this->data['page_title'] = 'Categories';
        // $this->load->model('admin/locations_model');
        // $this->load->model('admin/category_model');
    } 
	public function index()
	{
		//echo "Superadmin Controller";
		$this->load->view('dashboard');
	}
	public function profit_loss()
	{	$this->data['page_title'] = "Profit & Loss Form";
		$this->load->view('profit-loss-form',$this->data);
	}
}
?>