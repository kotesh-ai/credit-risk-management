<?php $this->load->view('header.php'); ?>

<section class="login-section"> 

    <div class="row no-gutters ">
        
        <div class="col-md-5">
            <div class="form-holder ">
                <div class="form-content">
                    <div class="form-items pb pt pl pr">
                        <div class="digi-logo pb" style="text-align: center;">
                          <img src="<?php echo base_url().'assets/images/digi-logo.png'; ?>" class="img-fluid center pb-3" width="100" height="100">
                        </div>
                        <div class="page-links">
                            <h5 class="text-center"><a href="#" class="active">Login</a></h5>
                            <p class="login-para pt pb text-center">Please enter your username and password</p>
                        </div>
                        <?php echo form_open(base_url(), 'method="post"');?>
                            <p class="text-center" style="text-align: center;margin: 8px;font-size: 12px;"><?php if($this->session->flashdata('msg')!="") {?>   
                                        <strong><?php echo $this->session->flashdata('msg');?></strong>
                                        <?php }?>
                                        </p>
                            
                            <input type="email" name="email" required placeholder="Username" class="form-control mb-3" id="email" value="<?php echo $this->input->post('email'); ?>" />
                                        <?php echo form_error('email'); ?>

                                        <input type="password" name="password" required placeholder="Password"  class="form-control mb-3" id="password"  value="<?php echo $this->input->post('password'); ?>"  />
                                        <?php echo form_error('password'); ?>
                            
                            <div class="form-button pb">
                                <button id="submit" type="submit" class="ibtn btn ">Login</button> 
                                
                                <!-- <a href="#">Forget password?</a> -->
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7 text-center justify-content-center">
            <div class="logo pb pt pl pr">
        
               <div class="test text-center">
                    <img class="logo-size img-fluid" src="<?php echo base_url().'assets/images/AJMS-PNG-Logo.png'; ?>" alt="Ajms Global">
               </div>
            </div>
        </div>
    </div>

    
  <div class="tab-content">
    <div id="login" class="tab-pane fade in active">
      <h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
    <div id="register" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
  </div>
</section>



<?php 
$this->load->view('footer.php'); ?>