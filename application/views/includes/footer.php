
</div>

<!-- For color picker -->
<script src="<?= base_url(); ?>admin_assets/js/jscolor.js"></script>

<!-- Mainly scripts -->
<script src="<?= base_url(); ?>admin_assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Date picker -->
<script src="<?= base_url(); ?>admin_assets/jquery-ui-1.12.1.custom/jquery-ui.js"></script>


<!-- Flot -->
<script src="<?= base_url(); ?>admin_assets/js/plugins/flot/jquery.flot.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/flot/jquery.flot.pie.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/flot/jquery.flot.symbol.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/flot/curvedLines.js"></script>

<!-- Peity -->
<script src="<?= base_url(); ?>admin_assets/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?= base_url(); ?>admin_assets/js/inspinia.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?= base_url(); ?>admin_assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Jvectormap -->
<script src="<?= base_url(); ?>admin_assets/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?= base_url(); ?>admin_assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Sparkline -->
<script src="<?= base_url(); ?>admin_assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?= base_url(); ?>admin_assets/js/demo/sparkline-demo.js"></script>

<!-- Sweet alert -->
<script src="<?= base_url(); ?>admin_assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- ChartJS-->
<script src="<?= base_url(); ?>admin_assets/js/plugins/chartJs/Chart.min.js"></script>

<script src="<?= base_url() ?>admin_assets/js/plugins/toastr/toastr.min.js"></script>

<!-- validate -->
<script src="<?= base_url() ?>admin_assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/validate/additional-methods.min.js"></script>
<!-- iCheck -->
<script src="<?= base_url() ?>admin_assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- blueimp gallery -->
<script src="<?= base_url() ?>admin_assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script src="<?=base_url()?>admin_assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Multi Select -->
<script src="<?=base_url()?>admin_assets/js/plugins/select2/select2.full.min.js"></script>
<script>
    $(document).ready(function () {
    $('#myTable').DataTable();
     $('.dataTables').DataTable({
            pageLength: 10, responsive: true,
        });
    $(".input-sm").attr('type','text');
    $(".input-sm").val('Search Here ');
        setTimeout(function () {
            toastr.options = {closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
<?php if ($this->session->flashdata('success_message')) { ?>
                toastr.success(<?= $this->session->flashdata('success_message') ?>);
<?php } ?>
<?php if ($this->session->flashdata('error_message')) { ?>
                toastr.error(<?= $this->session->flashdata('error_message') ?>);
<?php } ?>
<?php if ($this->session->flashdata('warning_message')) { ?>
                toastr.warning(<?= $this->session->flashdata('warning_message') ?>);
<?php } ?>
<?php /* <?= ((validation_errors()) && (null !== validation_errors())) ? "toastr.warning('".removeNewLine(removepTag(validation_errors()))."');":"" */ ?>
        }, 1300);
        //Date picker script starts here
    });
    $(function () {
        $('.number').on('keydown', function (e) {
            -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
    });</script>

<!-- <script src="<?= base_url() ?>admin_assets/js/plugins/dataTables/datatables.min.js"></script> -->


<!-- Page-Level Scripts -->
<script>
    $(document).on("click", '.logout', function () {
        var del_id = $(this).val();
        swal({
            title: "Are you sure",
            text: "You Want to Logout?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Logout!",
            closeOnConfirm: false
        },
        function () {
            window.location = "<?= base_url(); ?>login/logout";
        });
    });

 
</script>

<script>
    function check_file_size(val) {

        var uploadField = document.getElementById(val);
        var FileName = uploadField.files[0].name;
        var FileExtension = FileName.split('.')[FileName.split('.').length - 1]; //alert(FileExtension);
        if (!(FileExtension == 'pdf' || FileExtension == 'PDF' || FileExtension == 'svg' || FileExtension == 'SVG' || FileExtension == 'jpg' || FileExtension == 'JPG' || FileExtension == 'jpeg' || FileExtension == 'JPEG' || FileExtension == 'png' || FileExtension == 'PNG')) {
            swal({
                title: "Warning!",
                text: "Please Upload Image Files(PNG/JPG/JPEG) Only",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false
            });
            document.getElementById(val).value = "";
        } else if (uploadField.files[0].size > 7000000) {
            swal({
                title: "Warning!",
                text: "Max upload size 5 MB",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false
            });
            document.getElementById(val).value = "";
        }
    }

    var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $("input[type='number']").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                $(".error").css("display", ret ? "none" : "inline");
                return ret;
            });
            $(".numeric").bind("paste", function (e) {
                return false;
            });
            $(".numeric").bind("drop", function (e) {
                return false;
            });
        });

</script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });</script>
<script src="<?= base_url() ?>admin_assets/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">

function totalSales(pos){
    if ($.trim($("#gross_mf_sales_"+pos).val()) == ""){
        var gross_mf_sales = 0;
    }
    else{
        var gross_mf_sales = $.trim($("#gross_mf_sales_"+pos).val());
    }

    if ($.trim($("#gross_td_sales_"+pos).val()) == ""){
        var gross_td_sales = 0;
    }
    else{
        var gross_td_sales = $.trim($("#gross_td_sales_"+pos).val());
    }  
    var sum = parseInt(gross_mf_sales) + parseInt(gross_td_sales);
    return sum;
}

function netSales(pos){
    if ($.trim($("#total_sales_"+pos).val()) == ""){
        var total_sales = 0;
    }
    else{
        var total_sales = $.trim($("#total_sales_"+pos).val());
    }

    if ($.trim($("#exise_duty_"+pos).val()) == ""){
        var exise_duty = 0;
    }
    else{
        var exise_duty = $.trim($("#exise_duty_"+pos).val());
    }  
    var sum = parseInt(total_sales) + parseInt(exise_duty);
    return sum;
}

function totalIncome(pos){

    if ($.trim($("#net_sales_"+pos).val()) == ""){
        var net_sales = 0;
    }
    else{
        var net_sales = $.trim($("#net_sales_"+pos).val());
    }

    if ($.trim($("#other_inc_op_"+pos).val()) == ""){
        var other_inc_op = 0;
    }
    else{
        var other_inc_op = $.trim($("#other_inc_op_"+pos).val());
    }  
    var sum = parseInt(net_sales) + parseInt(other_inc_op);
    return sum;
}

function caseProduction(pos){

    if ($.trim($("#raw_mt_counsumed_"+pos).val()) == ""){
        var raw_mt_counsumed = 0;
    }
    else{
        var raw_mt_counsumed = $.trim($("#raw_mt_counsumed_"+pos).val());
    }

    if ($.trim($("#cons_st_include_pack_mt_"+pos).val()) == ""){
        var cons_st_include_pack_mt = 0;
    }
    else{
        var cons_st_include_pack_mt = $.trim($("#cons_st_include_pack_mt_"+pos).val());
    } 
    if ($.trim($("#pw_fuel_water_"+pos).val()) == ""){
        var pw_fuel_water = 0;
    }
    else{
        var pw_fuel_water = $.trim($("#pw_fuel_water_"+pos).val());
    } 
    if ($.trim($("#wg_sal_"+pos).val()) == ""){
        var wg_sal = 0;
    }
    else{
        var wg_sal = $.trim($("#wg_sal_"+pos).val());
    } 
    if ($.trim($("#oth_dir_exp_"+pos).val()) == ""){
        var oth_dir_exp = 0;
    }
    else{
        var oth_dir_exp = $.trim($("#oth_dir_exp_"+pos).val());
    }  
    var sum = parseInt(raw_mt_counsumed) + parseInt(cons_st_include_pack_mt) + parseInt(pw_fuel_water) + parseInt(wg_sal) + parseInt(oth_dir_exp);
    return sum;

}
 
function costOfGoodSold(pos){

    if ($.trim($("#case_production_"+pos).val()) == ""){
        var case_production = 0;
    }
    else{
        var case_production = $.trim($("#case_production_"+pos).val());
    }

    if ($.trim($("#pur_good_resale_"+pos).val()) == ""){
        var pur_good_resale = 0;
    }
    else{
        var pur_good_resale = $.trim($("#pur_good_resale_"+pos).val());
    }

    if ($.trim($("#dec_inc_wrp_fg_"+pos).val()) == ""){
        var dec_inc_wrp_fg = 0;
    }
    else{
        var dec_inc_wrp_fg = $.trim($("#dec_inc_wrp_fg_"+pos).val());
    }  
    var sum = parseInt(case_production) + parseInt(pur_good_resale) + parseInt(dec_inc_wrp_fg);
    return sum;
}

function operatingCost(pos){

    if ($.trim($("#commi_other_sel_exp_"+pos).val()) == ""){
        var commi_other_sel_exp = 0;
    }
    else{
        var commi_other_sel_exp = $.trim($("#commi_other_sel_exp_"+pos).val());
    }

    if ($.trim($("#adv_res_dev_prom_exp_"+pos).val()) == ""){
        var adv_res_dev_prom_exp = 0;
    }
    else{
        var adv_res_dev_prom_exp = $.trim($("#adv_res_dev_prom_exp_"+pos).val());
    }

    if ($.trim($("#admin_selling_exp_"+pos).val()) == ""){
        var admin_selling_exp = 0;
    }
    else{
        var admin_selling_exp = $.trim($("#admin_selling_exp_"+pos).val());
    }

    if ($.trim($("#other_exp_"+pos).val()) == ""){
        var other_exp = 0;
    }
    else{
        var other_exp = $.trim($("#other_exp_"+pos).val());
    }  

    if ($.trim($("#cost_good_sold_"+pos).val()) == ""){
        var cost_good_sold = 0;
    }
    else{
        var cost_good_sold = $.trim($("#cost_good_sold_"+pos).val());
    }    
    var sum = parseInt(commi_other_sel_exp) + parseInt(adv_res_dev_prom_exp) + parseInt(admin_selling_exp) + parseInt(other_exp) + parseInt(cost_good_sold);
    return sum;
}


function operatingProfitLossBefore(pos){
    var sum = parseInt(totalIncome(pos)) - parseInt(operatingCost(pos));
    return sum;
}

function operatingProfitLossAfter(pos){
    if ($.trim($("#int_long_term_debt_"+pos).val()) == ""){
        var int_long_term_debt = 0;
    }
    else{
        var int_long_term_debt = $.trim($("#int_long_term_debt_"+pos).val());
    }

    if ($.trim($("#intr_short_term_debt_fina_exp_"+pos).val()) == ""){
        var intr_short_term_debt_fina_exp = 0;
    }
    else{
        var intr_short_term_debt_fina_exp = $.trim($("#intr_short_term_debt_fina_exp_"+pos).val());
    }

    if ($.trim($("#lease_rent_"+pos).val()) == ""){
        var lease_rent = 0;
    }
    else{
        var lease_rent = $.trim($("#lease_rent_"+pos).val());
    }

    if ($.trim($("#depreciation_"+pos).val()) == ""){
        var depreciation = 0;
    }
    else{
        var depreciation = $.trim($("#depreciation_"+pos).val());
    }  

    if ($.trim($("#misc_expen_wr_off_year_"+pos).val()) == ""){
        var misc_expen_wr_off_year = 0;
    }
    else{
        var misc_expen_wr_off_year = $.trim($("#misc_expen_wr_off_year_"+pos).val());
    }    
    var sum = parseInt(int_long_term_debt) + parseInt(intr_short_term_debt_fina_exp) + parseInt(lease_rent) + parseInt(depreciation) + parseInt(misc_expen_wr_off_year);
    return sum;
}

function operatingProfitLoss(pos){
    var sum = parseInt(operatingProfitLossBefore(pos)) - parseInt(operatingProfitLossAfter(pos));
    return sum;
}

function profitBeforeTax(pos){

    if ($.trim($("#other_inc_"+pos).val()) == ""){
        var other_inc = 0;
    }
    else{
        var other_inc = $.trim($("#other_inc_"+pos).val());
    }

    if ($.trim($("#other_expences_"+pos).val()) == ""){
        var other_expences = 0;
    }
    else{
        var other_expences = $.trim($("#other_expences_"+pos).val());
    }

    if ($.trim($("#ext_odinary_inc_"+pos).val()) == ""){
        var ext_odinary_inc = 0;
    }
    else{
        var ext_odinary_inc = $.trim($("#ext_odinary_inc_"+pos).val());
    }

    if ($.trim($("#ext_odinary_expences_"+pos).val()) == ""){
        var ext_odinary_expences = 0;
    }
    else{
        var ext_odinary_expences = $.trim($("#ext_odinary_expences_"+pos).val());
    }  

    var sum = parseInt(operatingProfitLoss(pos)) + parseInt(other_inc) + parseInt(ext_odinary_inc) - parseInt(other_expences) - parseInt(ext_odinary_expences);
    return sum;

}

function profitLossAfterTax(pos){
    if ($.trim($("#profit_loss_after_tax_"+pos).val()) == ""){
        var profit_loss_after_tax = 0;
    }
    else{
        var profit_loss_after_tax = $.trim($("#profit_loss_after_tax_"+pos).val());
    }

    var sum = parseInt(profitBeforeTax(pos)) - parseInt(profit_loss_after_tax);
    return sum;
}

function profitLossBeforeTax(pos){

    if ($.trim($("#depreciation_"+pos).val()) == ""){
        var depreciation = 0;
    }
    else{
        var depreciation = $.trim($("#depreciation_"+pos).val());
    }  

    if ($.trim($("#ext_odinary_expences_"+pos).val()) == ""){
        var ext_odinary_expences = 0;
    }
    else{
        var ext_odinary_expences = $.trim($("#ext_odinary_expences_"+pos).val());
    }  

    if ($.trim($("#ext_odinary_inc_"+pos).val()) == ""){
        var ext_odinary_inc = 0;
    }
    else{
        var ext_odinary_inc = $.trim($("#ext_odinary_inc_"+pos).val());
    }

    var sum = parseInt(profitBeforeTax(pos)) + parseInt(depreciation) + parseInt(ext_odinary_expences)- parseInt(ext_odinary_inc);
    return sum;
}


 function calculate(val,pos){
    var id = val+"_"+pos;
    if(val == 'total_sales'){
        
        var sum = totalSales(pos);
        $("#"+id).val(sum);

        /* calculate net sales */
        var net_sum = netSales(pos);
        $("#net_sales_"+pos).val(net_sum);
        
        /* calculating total income */
        var tot_income = totalIncome(pos);
        $("#total_inc_"+pos).val(tot_income);

        /* calculating case production */
        var case_production = caseProduction(pos);
        $("#case_production_"+pos).val(case_production);

        /* calculating costOfGoodSold */
        var cost_good_sold = costOfGoodSold(pos);
        $("#cost_good_sold_"+pos).val(cost_good_sold);

        /* calculating operating cost */
        var opr_cost = operatingCost(pos);
        $("#opr_cost_"+pos).val(opr_cost);

        /* calculating operating profit Loss */
        var opr_profit = operatingProfitLossBefore(pos);
        $("#opr_profit_int_lr_dep_amort_"+pos).val(opr_profit);

        /* calculating operating Profit */
        var act_opt_profit = operatingProfitLoss(pos);
        $("#opr_profit_"+pos).val(act_opt_profit);

        /* calculating profit before tax */
        var pbt = profitBeforeTax(pos);
        $("#pbt_"+pos).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
        
    }

    // calculating net sales
    if(val == 'net_sales'){
        
        var net_sum = netSales(pos);
        $("#"+id).val(net_sum);

        /* calculating total income */
        var tot_income = totalIncome(pos);
        $("#total_inc_"+pos).val(tot_income);

        /* calculating case production */
        var case_production = caseProduction(pos);
        $("#case_production_"+pos).val(case_production);

        /* calculating costOfGoodSold */
        var cost_good_sold = costOfGoodSold(pos);
        $("#cost_good_sold_"+pos).val(cost_good_sold);

        /* calculating operating cost */
        var opr_cost = operatingCost(pos);
        $("#opr_cost_"+pos).val(opr_cost);

        /* calculating operating profit Loss */
        var opr_profit = operatingProfitLossBefore(pos);
        $("#opr_profit_int_lr_dep_amort_"+pos).val(opr_profit);

         /* calculating operating Profit */
        var act_opt_profit = operatingProfitLoss(pos);
        $("#opr_profit_"+pos).val(act_opt_profit);
       
       /* calculating profit before tax */
        var pbt = profitBeforeTax(pos);
        $("#pbt_"+pos).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
        
    }

    // calculating total income
    
    if(val == 'total_inc'){

        var tot_income = totalIncome(pos);
        $("#"+id).val(tot_income);

        /* calculating case production */
        var case_production = caseProduction(pos);
        $("#case_production_"+pos).val(case_production);

        /* calculating costOfGoodSold */
        var cost_good_sold = costOfGoodSold(pos);
        $("#cost_good_sold_"+pos).val(cost_good_sold);

        /* calculating operating cost */
        var opr_cost = operatingCost(pos);
        $("#opr_cost_"+pos).val(opr_cost);
        
        /* calculating operating profit Loss */
        var opr_profit = operatingProfitLossBefore(pos);
        $("#opr_profit_int_lr_dep_amort_"+pos).val(opr_profit);

         /* calculating operating Profit */
        var act_opt_profit = operatingProfitLoss(pos);
        $("#opr_profit_"+pos).val(act_opt_profit);

        /* calculating profit before tax */
        var pbt = profitBeforeTax(pos);
        $("#pbt_"+pos).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
        
    }

    // calculating case production

    if(val == 'case_production'){

        var case_production = caseProduction(pos);
        $("#"+id).val(case_production);

        /* calculating costOfGoodSold */
        var cost_good_sold = costOfGoodSold(pos);
        $("#cost_good_sold_"+pos).val(cost_good_sold);

        /* calculating operating cost */
        var opr_cost = operatingCost(pos);
        $("#opr_cost_"+pos).val(opr_cost);

        /* calculating operating profit Loss */
        var opr_profit = operatingProfitLossBefore(pos);
        $("#opr_profit_int_lr_dep_amort_"+pos).val(opr_profit);

         /* calculating operating Profit */
        var act_opt_profit = operatingProfitLoss(pos);
        $("#opr_profit_"+pos).val(act_opt_profit);
        
        /* calculating profit before tax */
        var pbt = profitBeforeTax(pos);
        $("#pbt_"+pos).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
        
    }

    // calculating cost of gold 
    if(val == 'cost_good_sold'){
        var cost_good_sold = costOfGoodSold(pos);
        $("#"+id).val(cost_good_sold);

        /* calculating operating cost */
        var opr_cost = operatingCost(pos);
        $("#opr_cost_"+pos).val(opr_cost);

        /* calculating operating profit Loss */
        var opr_profit = operatingProfitLossBefore(pos);
        $("#opr_profit_int_lr_dep_amort_"+pos).val(opr_profit);

         /* calculating operating Profit */
        var act_opt_profit = operatingProfitLoss(pos);
        $("#opr_profit_"+pos).val(act_opt_profit);

        /* calculating profit before tax */
        var pbt = profitBeforeTax(pos);
        $("#pbt_"+pos).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
    }

    // calculating operating cost
    if(val == 'opr_cost'){
        var opr_cost = operatingCost(pos);
        $("#"+id).val(opr_cost);

        /* calculating operating profit Loss */
        var opr_profit = operatingProfitLossBefore(pos);
        $("#opr_profit_int_lr_dep_amort_"+pos).val(opr_profit);

         /* calculating operating Profit */
        var act_opt_profit = operatingProfitLoss(pos);
        $("#opr_profit_"+pos).val(act_opt_profit);

        /* calculating profit before tax */
        var pbt = profitBeforeTax(pos);
        $("#pbt_"+pos).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
    }

    // calculating operating profit
    if(val == 'opr_profit'){
        var act_opt_profit = operatingProfitLoss(pos);
        $("#"+id).val(act_opt_profit);

        /* calculating profit before tax */
        var pbt = profitBeforeTax(pos);
        $("#pbt_"+pos).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
    }

    // calculating profit before tax
    if(val == 'pbt'){
        var pbt = profitBeforeTax(pos);
        $("#"+id).val(pbt);

        /* calculating profit Loss after tax */
        var profit_loss_after_depr_tax = profitLossAfterTax(pos);
        $("#profit_loss_after_tax_"+pos).val(profit_loss_after_depr_tax);

        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax_"+pos).val(profit_loss_before_depr_tax);
    }

    if(val == "profit_loss_after_tax"){
        var pat = profitLossAfterTax(pos);
        $("#"+id).val(pat);


        var profit_loss_before_depr_tax = profitLossBeforeTax(pos);
        $("#profit_loss_before_depr_tax"+pos).val(profit_loss_before_depr_tax);
    }

 }   

</script>
</body>
</html>
