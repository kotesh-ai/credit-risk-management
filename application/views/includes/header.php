<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $page_title; ?></title>
        <link rel="shortcut icon" href="<?= FILE_PATH ?>digi.png" />
        <link href="<?= base_url(); ?>admin_assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url(); ?>admin_assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?= base_url() ?>admin_assets/js/plugins/toastr/toastr.min.css" rel="stylesheet">
        </link>
        <link href="<?= base_url() ?>admin_assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>admin_assets/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="<?= base_url() ?>admin_assets/css/plugins/iCheck/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>admin_assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
        <link href="<?= base_url(); ?>admin_assets/css/animate.css" rel="stylesheet">

        <link href="<?= base_url(); ?>admin_assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url(); ?>admin_assets/css/custom.css" rel="stylesheet">

        <link href="<?= base_url(); ?>admin_assets/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet">
        <link href="<?= base_url(); ?>admin_assets/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

        <link href="<?= base_url(); ?>admin_assets/css/plugins/select2/select2.min.css" rel="stylesheet">

        <script src="<?= base_url(); ?>admin_assets/js/jquery-2.1.1.js"></script>
    </head>
    <style>
    input:-webkit-autofill {
       -webkit-box-shadow: 0 0 0 1000px white inset !important;
     }
     .text-red{
        color: red;
     }
     .f-head{
        font-size: 16px;
     }
     .m-2{
        margin: 0.2rem;
     }
    </style>
    <body>
        <div id="wrapper">
             <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element" >
                                <span>
                                    <img alt="image" class="img-responsive" src="<?= FILE_PATH ?>digi.png"  style="margin: 0 auto;display: block;height: 65px;"/>
                                </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear text-center">
                                        <span class="block m-t-xs">
                                            <strong class="font-bold">
                                                <?= $this->session->userdata('username') ?>
                                                <b class="caret">
                                                </b>
                                            </strong>
                                        </span>
                                    </span>
                                </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <!-- 
                                    <li>
                                        <a href="<?= base_url(); ?>admin/settings">Profile Settings
                                        </a>
                                    </li>
                                    <?php if ($this->session->userdata('user_type') == 'admin') { ?>
                                        <li>
                                            <a href="<?= base_url(); ?>admin/Db_backup">DB Backup
                                            </a>
                                        </li>
                                    <?php } ?>
                                    <li class="divider">
                                    </li>
                                     -->
                                    <li>
                                        <a class="logout" href="javascript:void(0)" >Logout
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                <img alt="image" class="img-md img-responsive" src="<?= FILE_PATH ?>digi.png" />
                            </div>
                        </li>
                         <li class="<?=($page == 'dashboard') ? 'active' : ''?>">
                            <a href="<?=base_url()?>dashboard">
                                <i class="fa fa-dashboard">
                                </i>
                                <span class="nav-label">Dashboard
                                </span>
                            </a>
                        </li>
                        <li class="<?= ($page == 'Profit&Loss') ? 'active' : ''?>">
                            <a href="<?=base_url()?>ProfitLoss">
                                <i class="fa fa-dashboard">
                                </i>
                                <span class="nav-label">Profit & Loss Calculation
                                </span>
                            </a>
                        </li>
                        <li class="">
                            <a href="<?=base_url()?>login/logout">
                                <i class="fa fa-dashboard">
                                </i>
                                <span class="nav-label">Logout
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                                <i class="fa fa-bars">
                                </i>
                            </a>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">

                            <li>
                                <a href="javascript:void(0)" style="padding-bottom: 0px;padding-top: 0px">
                                    <img src="<?= FILE_PATH?>digi.png" style="height: 40px; width: 40px;" class="img-thumbnail" />
                                    <?= $this->session->userdata('user_name'); ?></a>
                            </li>
                            <li>
                                <a class="logout" href="javascript:void(0)" title="Log out" style="">
                                    <i class="fa fa-sign-out">
                                    </i>Logout
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            
