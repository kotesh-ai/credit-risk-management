<?php
$this->load->view('includes/header');
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="form-group row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="b-s-heading text-center">
                        <h4>FEEDING OF COMPLETE BALANCE SHEET DATA</h4>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                                <th>Sr. No.</th>
                                <th>ASSETS</th>
                                <th>
                                    <select class="form-control" id="year1">
                                        <?php for ($x = 2001; $x <= 2030; $x++) { ?>
                                           <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                        <?php }?>
                                    </select>
                                </th>
                                <th>
                                    <select class="form-control" id="year2">
                                        <?php for ($x = 2001; $x <= 2030; $x++) { ?>
                                           <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                        <?php }?>
                                    </select>  
                                </th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>
                                    <select class="form-control" id="curruncy1">
                                        <?php foreach ($curruncy as $value) { ?>
                                            <option value=""><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select> 
                                </th>
                                <th>
                                    <select class="form-control" id="curruncy2">
                                        <?php foreach ($curruncy as $value) { ?>
                                            <option value=""><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>   
                                </th>
                            </tr>
                            
                          <tr>
                              <td colspan="4">
                                   <div class="col-md-12 text-red f-head">
                                       Fixed Assets
                                    </div>
                              </td>
                          </tr>
                          <tr>
                                <td>1.</td>
                                <td>Land & Building</td>
                                <td><input type="number" name="" min=0 class="form-control m-2" id=""></td>
                                <td><input type="number" name="" min=0 class="form-control m-2" id=""></td>
                          </tr>
                          
                          <tr>
                                <td>2.</td>
                                <td>Plant & Machinery</td>
                                <td> 
                                    <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                          </tr>
                          <tr>
                                <td>3.</td>
                                <td>Others including capital work-in-progress</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                          </tr>
                          <tr>
                                <th>4.</th>
                                <th>Gross Fixed Assets (Sum 1 to 3)</th>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                  <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                           </tr>
                           <tr>
                                <td>5.</td>
                                <td>Depreciation including   accumulated depereciation not provided for</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                           </tr>
                           <tr>
                                <th>6.</th>
                                <th>Depreciated value of Fixed Assets(4-5)</th>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                          </tr>
                          <tr>
                                <td>7.</td>
                                <td>Net value of leased assets (if shown as a part of gross fixed assets)</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                          </tr>
                          <tr>
                                <th>8.</th>
                                <th>Net Fixed Assets(6-7)</th>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                          </tr>
                          <tr>
                              <td colspan="4">
                                  <div class="form-group row">
                                      <div class="col-md-12 text-red">
                                         <u>Investment</u>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                          <tr>
                                <td>9.</td>
                                <td>Investment in Group/associate companies</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                          </tr>
                          <tr>
                                <td>10.</td>
                                <td>Other investment (other than marketable securities)</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                          </tr>
                          <tr>
                                <td>11.</td>
                                <td>Loans and advances to group companies/other companies</td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                          </tr>
                          <tr>
                                <td>12.</td>
                                <td>Security Deposits with Govt departments/agencies</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                          </tr>
                          <tr>
                                <th>13.</th>
                                <th>Total of investments (Sum 9 to 12)</th>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="">
                                </td>
                          </tr>
                          <tr>
                              <td colspan="4">
                                  <div class="form-group row">
                                      <div class="col-md-12 text-red">
                                         <u>Current Assets</u>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                          <tr>
                                <td>14.</td>
                                <td>Raw materials</td>
                                <td>
                                    <input type="number"  name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number"  name="" min=0 class="form-control m-2" id="" >
                                </td>
                          </tr>
                          <tr>
                                <td>15.</td>
                                <td>Finished goods including stocks on hire purchase</td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>16.</td>
                                <td> Work-in-progress</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>17.</td>
                                <td> Stores,Spares & Packaging materials</td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                      <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>18.</td>
                                <td> Receivables</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>19.</td>
                                <td>Loans & Advances(Other than group/associate companies)</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>20.</td>
                                <td> Investment in marketable securities(Other than group/associate companies)</td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>21.</td>
                                <td>Advance tax(net of provision)</td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>22.</td>
                                <td>Cash & Bank balances</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <th>23.</th>
                                <th>Others Total(24+25+26)</th>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>24.</td>
                                <td>Others (1)</td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>25.</td>
                                <td>Others(2)</td>
                                <td>
                                     <input type="number"  name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number"  name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>

                            <tr>
                                <td>26.</td>
                                <td>Others(3)</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>

                            <tr>
                                <th>27.</th>
                                <th>Total  Current Assets (Sum 14 to 23)</th>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                  <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>28.</td>
                                <td>Total Assets(6+13+27)</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                     <div class="col-md-12 text-red f-head">
                                          LIABILITIES
                                      </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           <u>ShareCapital</u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>29.</td>
                                <td>Equity</td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>30</td>
                                <td>Preference Share</td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>31.</td>
                                <td>Advance against share capital/ application money/ Quasi Capital (Unsecured debts having characteristic of capital in case of proprietorship/ partnership firms not exceeding 100% of capital)</td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <th>32.</th>
                                <th>Total Share Capital (29+ 31)</th>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           <u>ReserveAndSurplus</u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>33.</td>
                                <td>Capital Reserves</td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>34.</td>
                                <td>General Reserves</td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                                <td>
                                   <input type="number" name="" min=0 class="form-control m-2" id="" >
                                </td>
                            </tr>
                            <tr>
                                <td>35.</td>
                                <td>Subsidy</td>
                                <td>
                                     <input type="number" name="" min=0 class="form-control m-2" id=""  >
                                </td>
                                <td>
                                    <input type="number" name="" min=0 class="form-control m-2" id=""  >
                                </td>
                            </tr>
                            <tr>
                                <td>36.</td>
                                <td>Other reserve including Deferred Tax Liabilities but excluding Revaluation reserves</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>37.</th>
                                <th>Sub-total(Sum 33 to 36)</th>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>38.</td>
                                <td>Revaluation Reserves</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>

                            <tr>
                                <th>39.</th>
                                <th>Total Reserves & Surplus(37+ 38)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           <u>Intangible Assets & Losses</u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>40.</td>
                                <td>Accumulated Losses</td>
                                <td>
                                      <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>41.</td>
                                <td>Misc. expenses not written off</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>42.</td>
                                <td>Intangible Assets including Deferred Tax Assets</td>
                                <td>
                                      <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>43.</td>
                                <td>Accumulated depereciation not provided for</td>
                                <td>
                                      <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>

                            <tr>
                                <th>44.</th>
                                <th>Total Intangible Assets(Sum 40 t0 43)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>45.</th>
                                <th>Tangible Net Worth ((32+37)-44)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                     <div class="col-md-12 text-red f-head">
                                          Long Term Debts
                                      </div>
                                </td>
                            </tr>
                            <tr>
                                <td>46.</td>
                                <td>Term 0 ans from Banks</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>47.</td>
                                <td>Term 0 ans from Financial Institution</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>48.</td>
                                <td>Foreign Currency borrowings</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>49.</td>
                                <td>Debertures</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>50.</td>
                                <td>Fixed Depposits</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>51.</td>
                                <td>Long term loans from Promotors/Directors/Group Companies</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>52.</td>
                                <td>Other long term debts</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>53.</th>
                                <th>Total Long term debts(Sum 46 to 52 + 30)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           Short Term Debts
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>54.</td>
                                <td>Short Term Bank Borrowings</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>55.</td>
                                <td>Commercial Papers</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>56.</td>
                                <td>Foreign Currency borrowings</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>57.</td>
                                <td>Long from corporate bodies/ICDs(including group/associate companies)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>58.</td>
                                <td>Other short term debts</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>59.</th>
                                <th>Total Short Term Debts (Sum 54 to 58)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>60.</th>
                                <th>Total Borrowings (53 + 59)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           <u>Current Liabilities & Provisions</u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>61.</td>
                                <td>Sundry Creditors</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>62.</td>
                                <td>Interest Accrued & due</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>63.</td>
                                <td>Provisions(Net)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>64.</td>
                                <td>Other current liabilities(including advance against capital work in progress)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>65.</th>
                                <th>Total of Other Current Liab. & Provns. (Sum 61 to 64)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>66.</th>
                                <th>Total Current Liabilities (59 + 65)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>67.</th>
                                <th>Total Outside Liabilities (60 + 65)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>68.</th>
                                <th>Total Liabilities (38 + 45 + 67)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <th>69.</th>
                                <th>Difference</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           <u>Other Information</u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>70.</td>
                                <td>Net value of Machinery(ies) sold during the year</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>71.</td>
                                <td>Net value of Investment sold during the year</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           <u>Contingent Liabilities constitution of</u>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>72.</td>
                                <td>Statutory Liabilities</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>73.</td>
                                <td>Liabilities against Bills Discounted</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>74.</td>
                                <td>Disputed Tax</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>75.</td>
                                <td>LC/LG/Corporate Gurantee</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            <tr>
                                <td>76.</td>
                                <td>Term debt due within one year</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="" name="">
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="4"> 
                                    <div class="form-group row">
                                        <div class="col-md-12 justify-content-center text-center">
                                            <button class="btn btn-info">Calculate</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->load->view('includes/footer');
?>
<script type="text/javascript">
</script>



