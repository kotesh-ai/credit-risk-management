<?php
$this->load->view('includes/header');
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="form-group row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="p-l-heading text-center">
                        <h4>FEEDING OF COMPLETE PROFIT & LOSS DATA</h4>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                                <th>Sr. No.</th>
                                <th></th>
                                <th>
                                    <select class="form-control" id="year1">
                                        <?php for ($x = 2001; $x <= 2030; $x++) { ?>
                                           <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                        <?php }?>
                                    </select>
                                </th>
                                <th>
                                    <select class="form-control" id="year2">
                                        <?php for ($x = 2001; $x <= 2030; $x++) { ?>
                                           <option value="<?php echo $x; ?>"><?php echo $x; ?></option>
                                        <?php }?>
                                    </select>  
                                </th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>
                                    <select class="form-control" id="curruncy1">
                                        <?php foreach ($curruncy as $value) { ?>
                                            <option value=""><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select> 
                                </th>
                                <th>
                                    <select class="form-control" id="curruncy2">
                                        <?php foreach ($curruncy as $value) { ?>
                                            <option value=""><?php echo $value; ?></option>
                                        <?php } ?>
                                    </select>   
                                </th>
                            </tr>
                          <tr>
                                <td>1.</td>
                                <td>Please fill here Profit before tax for tallying</td>
                                <td><input type="number" min=0 class="form-control m-2" id="prof_before_tax_tallying_1"></td>
                                <td><input type="number" min=0 class="form-control m-2" id="prof_before_tax_tallying_2"></td>
                          </tr>
                          <tr>
                              <td colspan="4">
                                   <div class="col-md-12 text-red f-head">
                                        Gross Sales
                                    </div>
                              </td>
                          </tr>
                          <tr>
                                <td>2.</td>
                                <td>Manufacturing Sales(including job works)</td>
                                <td> 
                                    <input type="number" min=0 class="form-control m-2" id="gross_mf_sales_1" onkeyup="calculate('total_sales','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="gross_mf_sales_2" onkeyup="calculate('total_sales','2')">
                                </td>
                          </tr>
                          <tr>
                                <td>3.</td>
                                <td>Trading Sales</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="gross_td_sales_1" onkeyup="calculate('total_sales','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="gross_td_sales_2" onkeyup="calculate('total_sales','2')">
                                </td>
                          </tr>
                          <tr>
                                <th>4.</th>
                                <th>Total Sales (2+3)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="total_sales_1" readonly>
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="total_sales_2" readonly>
                                </td>
                           </tr>
                           <tr>
                                <td>5.</td>
                                <td>Tax</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="exise_duty_1" onkeyup="calculate('net_sales','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="exise_duty_2" onkeyup="calculate('net_sales','2')">
                                </td>
                           </tr>
                           <tr>
                                <th>6.</th>
                                <th>Net Sales (4+5)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="net_sales_1" readonly>
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="net_sales_2" readonly>
                                </td>
                          </tr>
                          <tr>
                                <td>7.</td>
                                <td>Other income operatoinal</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="other_inc_op_1" onkeyup="calculate('total_inc','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="other_inc_op_2" onkeyup="calculate('total_inc','1')">
                                </td>
                          </tr>
                          <tr>
                                <th>8.</th>
                                <th>Total Income (6+7)</th>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="total_inc_1" readonly>
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="total_inc_2" readonly>
                                </td>
                          </tr>
                          <tr>
                                <td>9.</td>
                                <td>Raw material consumesd</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="raw_mt_counsumed_1" onkeyup="calculate('case_production','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="raw_mt_counsumed_2" onkeyup="calculate('case_production','2')">
                                </td>
                          </tr>
                          <tr>
                                <td>10.</td>
                                <td>Consumable stores including packing material</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="cons_st_include_pack_mt_1" onkeyup="calculate('case_production','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="cons_st_include_pack_mt_2" onkeyup="calculate('case_production','2')">
                                </td>
                          </tr>
                          <tr>
                                <td>11.</td>
                                <td>Power,fuel & water</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="pw_fuel_water_1" onkeyup="calculate('case_production','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="pw_fuel_water_2" onkeyup="calculate('case_production','2')">
                                </td>
                          </tr>
                          <tr>
                                <td>12.</td>
                                <td>Wages & Salary</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="wg_sal_1" onkeyup="calculate('case_production','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="wg_sal_2" onkeyup="calculate('case_production','2')">
                                </td>
                          </tr>
                          <tr>
                                <td>13.</td>
                                <td>Other Direct Expenses</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="oth_dir_exp_1" onkeyup="calculate('case_production','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="oth_dir_exp_2" onkeyup="calculate('case_production','2')">
                                </td>
                          </tr>
                          <tr>
                                <th>14.</th>
                                <th>Cose of Production (Sum 9 to 13)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="case_production_1" readonly>
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="case_production_2" readonly>
                                </td>
                          </tr>
                          <tr>
                                <td>15.</td>
                                <td>Purchase of goods for resale</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="pur_good_resale_1" onkeyup="calculate('cost_good_sold','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="pur_good_resale_2"  onkeyup="calculate('cost_good_sold','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>16.</td>
                                <td> Decrease/(Increase) in work-in-progress(WIP) and finished goods(FG)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="dec_inc_wrp_fg_1"  onkeyup="calculate('cost_good_sold','1')">
                                </td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="dec_inc_wrp_fg_2"  onkeyup="calculate('cost_good_sold','2')">
                                </td>
                            </tr>
                            <tr>
                                <th>17.</th>
                                <th>  Cost of goods sold (Sum 14 to 16)</th>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="cost_good_sold_1" readonly>
                                </td>
                                <td>
                                      <input type="number" min=0 class="form-control m-2" id="cost_good_sold_2" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>18.</td>
                                <td> Admin & Selling Expenses</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="admin_selling_exp_1" onkeyup="calculate('opr_cost','1')">
                                </td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="admin_selling_exp_2" onkeyup="calculate('opr_cost','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>19.</td>
                                <td> Advertising research & development and Promotional expenses</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="adv_res_dev_prom_exp_1" onkeyup="calculate('opr_cost','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="adv_res_dev_prom_exp_2" onkeyup="calculate('opr_cost','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>20.</td>
                                <td> Commission & other Selling expenses</td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="commi_other_sel_exp_1" onkeyup="calculate('opr_cost','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="commi_other_sel_exp_2" onkeyup="calculate('opr_cost','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>21.</td>
                                <td> Other expenses(operational)</td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="other_exp_1" onkeyup="calculate('opr_cost','1')">
                                </td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="other_exp_2" onkeyup="calculate('opr_cost','2')">
                                </td>
                            </tr>
                            <tr>
                                <th>22.</th>
                                <th> Operating Cost (Sum 17 to 21)</th>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="opr_cost_1" readonly>
                                </td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="opr_cost_2" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th>23.</th>
                                <th> Operating Profit/(Loss) before interest, lease rentals, depreciation & amortisation (8-22)</th>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="opr_profit_int_lr_dep_amort_1" readonly>
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="opr_profit_int_lr_dep_amort_2" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>24.</td>
                                <td>Interest on long term debt</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="int_long_term_debt_1" onkeyup="calculate('opr_profit','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="int_long_term_debt_2" onkeyup="calculate('opr_profit','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>25.</td>
                                <td>Interest on short term debt & financial expenses</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="intr_short_term_debt_fina_exp_1" onkeyup="calculate('opr_profit','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="intr_short_term_debt_fina_exp_2" onkeyup="calculate('opr_profit','2')">
                                </td>
                            </tr>

                            <tr>
                                <td>26.</td>
                                <td>Lease rentals</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="lease_rent_1" onkeyup="calculate('opr_profit','1')">
                                </td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="lease_rent_2" onkeyup="calculate('opr_profit','2')">
                                </td>
                            </tr>

                            <tr>
                                <td>27.</td>
                                <td>Depreciation</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="depreciation_1" onkeyup="calculate('opr_profit','1')">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="depreciation_2" onkeyup="calculate('opr_profit','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>28.</td>
                                <td>Misc expenditure writtenoff off during the year</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="misc_expen_wr_off_year_1" onkeyup="calculate('opr_profit','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="misc_expen_wr_off_year_2" onkeyup="calculate('opr_profit','2')">
                                </td>
                            </tr>
                            <tr>
                                <th>29.</th>
                                <th>Operating Profit/(Loss) ( 23- (sum 24 to 28))</th>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="opr_profit_1" readonly>
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="opr_profit_2" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>30</td>
                                <td>Other income(non-operatinal e.g dividend received,interest earned etc.)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="other_inc_1" onkeyup="calculate('pbt','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="other_inc_2" onkeyup="calculate('pbt','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>31.</td>
                                <td>Other expenses (non-operational)</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="other_expences_1" onkeyup="calculate('pbt','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="other_expences_2" onkeyup="calculate('pbt','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>32.</td>
                                <td>Extra odinary income (profit on sale of assets/investments,provisions written back etc.)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="ext_odinary_inc_1" onkeyup="calculate('pbt','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="ext_odinary_inc_2" onkeyup="calculate('pbt','2')">
                                </td>
                            </tr>
                            <tr>
                                <td>33.</td>
                                <td>Extra ordinary expenses(Loss on sale of asstes/investments etc.)</td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="ext_odinary_expences_1" onkeyup="calculate('pbt','1')">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="ext_odinary_expences_2" onkeyup="calculate('pbt','2')">
                                </td>
                            </tr>
                            <tr>
                                <th>34.</th>
                                <th>Profit before tax/(loss)(PBT)(29+30+32-31-33)</th>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="pbt_1" readonly>
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="pbt_2" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>35.</td>
                                <td> Tax</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="tax_1"  onkeyup="calculate('profit_loss_after_tax','1')">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="tax_2"  onkeyup="calculate('profit_loss_after_tax','2')">
                                </td>
                            </tr>
                            <tr>
                                <th>36.</th>
                                <th>Profit/(Loss) after tax (PAT)(34-35)</th>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="profit_loss_after_tax_1" readonly>
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="profit_loss_after_tax_2" readonly>
                                </td>
                            </tr>
                            <tr>
                                <th>37.</th>
                                <th>Profit/Loss Before Depreciation and Tax (PBDT)(34+27+33-32)</th>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="profit_loss_before_depr_tax_1" readonly>
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="profit_loss_before_depr_tax_2" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>38.</td>
                                <td>Difference</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="difference_1">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="difference_2">
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                    <div class="form-group row">
                                        <div class="col-md-12 text-red">
                                           <u>OTHER INFORMATION</u>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>39.</td>
                                <td>Non cash income(Included in 30 above)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="non_cash_inc_1">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="non_cash_inc_2">
                                </td>
                            </tr>
                            <tr>
                                <td>40.</td>
                                <td>Expenditure capitalised during the year</td>
                                <td>
                                      <input type="number" min=0 class="form-control m-2" id="expend_capit_during_year_1">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="expend_capit_during_year_2">
                                </td>
                            </tr>
                            <tr>
                                <td>41.</td>
                                <td>Interest capitalised during the year</td>
                                <td>
                                     <input type="number" min=0 class="form-control m-2" id="intrest_capit_during_year_1">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="intrest_capit_during_year_2">
                                </td>
                            </tr>
                            <tr>
                                <td>42.</td>
                                <td>Dividend paid</td>
                                <td>
                                      <input type="number" min=0 class="form-control m-2" id="dividend_paid_1">
                                </td>
                                <td>
                                   <input type="number" min=0 class="form-control m-2" id="dividend_paid_2">
                                </td>
                            </tr>
                            <tr>
                                <td>43.</td>
                                <td>Dividend received (included in 30 above)</td>
                                <td>
                                      <input type="number" min=0 class="form-control m-2" id="dividend_received_1">
                                </td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="dividend_received_2">
                                </td>
                            </tr>

                            <tr>
                                <td>44.</td>
                                <td>Interest received (included in 30 above)</td>
                                <td>
                                    <input type="number" min=0 class="form-control m-2" id="interest_received_1">
                                </td>
                                <td>
                                  <input type="number" min=0 class="form-control m-2" id="interest_received_2">
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4"> 
                                    <div class="form-group row">
                                        <div class="col-md-12 justify-content-center text-center">
                                            <button class="btn btn-info">Calculate</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- <div class="wrapper wrapper-content animated fadeInRight">
    <div class="form-group row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="">
                    <div class="form-group row text-red f-head">
                        <div class="col-md-5 text-red">
                        FEEDING OF COMPLETE PROFIT & LOSS DATA
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <p> 2006</p>
                               <p><small>Rs. Lakhs</small></p>
                            </div>
                            <div class="col-md-6">
                                <p> 2005</p>
                                <p><small>Rs. Lakhs</small></p>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red text-red">
                        1. Please fill here Profit before tax for tallying
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="prof_before_tax_tallying_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="prof_before_tax_tallying_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        
                        <div class="col-md-12 text-red f-head">
                            Gross Sales
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                    2. Manufacturing Sales(including job works)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="gross_mf_sales_1" onkeyup="calculate('total_sales','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="gross_mf_sales_2" onkeyup="calculate('total_sales','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        3. Trading Sales
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="gross_td_sales_1" onkeyup="calculate('total_sales','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="gross_td_sales_2" onkeyup="calculate('total_sales','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        4. Total Sales (2+3)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="total_sales_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="total_sales_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        5. Excise Duty
                        
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="exise_duty_1" onkeyup="calculate('net_sales','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="exise_duty_2" onkeyup="calculate('net_sales','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        6. Net Sales (4+5)
                         

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="net_sales_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="net_sales_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        7. Other income operatoinal
                        
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="other_inc_op_1" onkeyup="calculate('total_inc','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="other_inc_op_2" onkeyup="calculate('total_inc','1')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        8. Total Income (6+7)

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="total_inc_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="total_inc_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        9. Raw material consumesd
                        

                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="raw_mt_counsumed_1" onkeyup="calculate('case_production','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="raw_mt_counsumed_2" onkeyup="calculate('case_production','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        10. Consumable stores including packing material
                        
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="cons_st_include_pack_mt_1" onkeyup="calculate('case_production','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="cons_st_include_pack_mt_2" onkeyup="calculate('case_production','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        11. Power,fuel & water
                        
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="pw_fuel_water_1" onkeyup="calculate('case_production','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="pw_fuel_water_2" onkeyup="calculate('case_production','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        12. Wages & Salary
                        
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="wg_sal_1" onkeyup="calculate('case_production','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="wg_sal_2" onkeyup="calculate('case_production','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        13. Other Direct Expenses
                        
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="oth_dir_exp_1" onkeyup="calculate('case_production','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="oth_dir_exp_2" onkeyup="calculate('case_production','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        14. Cose of Production (Sum 9 to 13)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="case_production_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="case_production_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        15. Purchase of goods for resale
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="pur_good_resale_1" onkeyup="calculate('cost_good_sold','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="pur_good_resale_2"  onkeyup="calculate('cost_good_sold','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        16. Decrease/(Increase) in work-in-progress(WIP) and finished goods(FG)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="dec_inc_wrp_fg_1"  onkeyup="calculate('cost_good_sold','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="dec_inc_wrp_fg_2"  onkeyup="calculate('cost_good_sold','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        17. Cost of goods sold (Sum 14 to 16)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="cost_good_sold_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="cost_good_sold_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        18. Admin & Selling Expenses
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="admin_selling_exp_1" onkeyup="calculate('opr_cost','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="admin_selling_exp_2" onkeyup="calculate('opr_cost','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        19. Advertising research & development and Promotional expenses
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="adv_res_dev_prom_exp_1" onkeyup="calculate('opr_cost','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="adv_res_dev_prom_exp_2" onkeyup="calculate('opr_cost','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        20. Commission & other Selling expenses
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="commi_other_sel_exp_1" onkeyup="calculate('opr_cost','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="commi_other_sel_exp_2" onkeyup="calculate('opr_cost','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        21. Other expenses(operational)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="other_exp_1" onkeyup="calculate('opr_cost','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="other_exp_2" onkeyup="calculate('opr_cost','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        22. Operating Cost (Sum 17 to 21)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="opr_cost_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="opr_cost_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        23. Operating Profit/(Loss) before interest, lease rentals, depreciation & amortisation (8-22)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="opr_profit_int_lr_dep_amort_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="opr_profit_int_lr_dep_amort_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        24. Interest on long term debt
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="int_long_term_debt_1" onkeyup="calculate('opr_profit','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="int_long_term_debt_2" onkeyup="calculate('opr_profit','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        25. Interest on short term debt & financial expenses
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="intr_short_term_debt_fina_exp_1" onkeyup="calculate('opr_profit','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="intr_short_term_debt_fina_exp_2" onkeyup="calculate('opr_profit','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        26. Lease rentals
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="lease_rent_1" onkeyup="calculate('opr_profit','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="lease_rent_2" onkeyup="calculate('opr_profit','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        27. Depreciation
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="depreciation_1" onkeyup="calculate('opr_profit','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="depreciation_2" onkeyup="calculate('opr_profit','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        28. Misc expenditure writtenoff off during the year
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="misc_expen_wr_off_year_1" onkeyup="calculate('opr_profit','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="misc_expen_wr_off_year_2" onkeyup="calculate('opr_profit','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        29. Operating Profit/(Loss) ( 23- (sum 24 to 28))
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="opr_profit_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="opr_profit_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        30. Other income(non-operatinal e.g dividend received,interest earned etc.)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="other_inc_1" onkeyup="calculate('pbt','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="other_inc_2" onkeyup="calculate('pbt','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        31. Other expenses (non-operational)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="other_expences_1" onkeyup="calculate('pbt','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="other_expences_2" onkeyup="calculate('pbt','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        32. Extra odinary income (profit on sale of assets/investments,provisions written back etc.)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="ext_odinary_inc_1" onkeyup="calculate('pbt','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="ext_odinary_inc_2" onkeyup="calculate('pbt','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        33. Extra ordinary expenses(Loss on sale of asstes/investments etc.)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="ext_odinary_expences_1" onkeyup="calculate('pbt','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="ext_odinary_expences_2" onkeyup="calculate('pbt','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        34. Profit before tax/(loss)(PBT)(29+30+32-31-33)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="pbt_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="pbt_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        35. Tax
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="tax_1"  onkeyup="calculate('profit_loss_after_tax','1')">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="tax_2"  onkeyup="calculate('profit_loss_after_tax','2')">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        36. Profit/(Loss) after tax (PAT)(34-35)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="profit_loss_after_tax_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="profit_loss_after_tax_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red f-head">
                        37. Profit/Loss Before Depreciation and Tax (PBDT)(34+27+33-32)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="profit_loss_before_depr_tax_1" readonly>
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="profit_loss_before_depr_tax_2" readonly>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        38. Difference
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="difference_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="difference_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 text-red">
                        <u>OTHER INFORMATION</u>
                        </div>
                        

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        39. Non cash income(Included in 30 above)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="non_cash_inc_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="non_cash_inc_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        40. Expenditure capitalised during the year
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="expend_capit_during_year_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="expend_capit_during_year_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        41. Interest capitalised during the year
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="intrest_capit_during_year_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="intrest_capit_during_year_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        42. Dividend paid
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="dividend_paid_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="dividend_paid_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        43. Dividend received (included in 30 above)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="dividend_received_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="dividend_received_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-5 text-red">
                        44. Interest received (included in 30 above)
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-6">
                               <input type="number" min=0 class="form-control m-2" id="interest_received_1">
                            </div>
                            <div class="col-md-6">
                                <input type="number" min=0 class="form-control m-2" id="interest_received_2">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 justify-content-center text-center">
                            <button class="btn btn-info">Calculate</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
 -->

<?php
$this->load->view('includes/footer');
?>
<script type="text/javascript">
</script>



